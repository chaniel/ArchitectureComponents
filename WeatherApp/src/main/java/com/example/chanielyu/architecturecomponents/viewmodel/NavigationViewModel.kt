package com.example.chanielyu.architecturecomponents.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.support.v4.app.Fragment
import kotlin.reflect.KClass

class NavigationViewModel : ViewModel() {
    internal val navigationObservable: MutableLiveData<KClass<out Fragment>> = MutableLiveData()
}