package com.example.chanielyu.architecturecomponents.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.example.chanielyu.architecturecomponents.model.WeatherModel
import com.example.chanielyu.architecturecomponents.retrofitclient.WeatherRetrofitClient
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class WeatherViewModel constructor(client: WeatherRetrofitClient) : ViewModel() {
    internal val observableWeather: MutableLiveData<WeatherModel> = MutableLiveData()
    internal val observableProgress: MutableLiveData<Boolean> = MutableLiveData()
    private val weatherClient = client
    private var disposable: Disposable? = null

    init {
        loadWeather()
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.takeIf { it.isDisposed }?.dispose()
    }

    fun loadWeather() {
        disposable = weatherClient.getWeatherRx((-33.87190570512753).toString(), 151.209578141570.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .doOnSubscribe({
                    observableProgress.postValue(true)
                })
                .doAfterTerminate({
                    observableProgress.postValue(false)
                })
                .subscribe({ weatherModel ->
                    observableWeather.postValue(weatherModel)
                })
    }
}