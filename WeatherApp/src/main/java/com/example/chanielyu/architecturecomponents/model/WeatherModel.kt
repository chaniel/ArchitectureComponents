package com.example.chanielyu.architecturecomponents.model

import java.io.Serializable

class WeatherModel : Serializable {
    var latitude: Double? = null
    var longitude: Double? = null
    var timezone: String? = null
    var currently: WeatherDetails? = null
    var hourly: WeatherHourly? = null

    class WeatherHourly {
        var summary: String? = null
        var icon: String? = null
        var data: List<WeatherDetails>? = null
    }

    class WeatherDetails : Serializable {
        var time: Long = 0
        var summary: String? = null
        var icon: String? = null
        // "precipIntensity": 0.0079,
        // "precipProbability": 0.34,
        // "precipType": "rain",
        var temperature: Double = 0.toDouble()
        // "apparentTemperature": 60.53,
        // "dewPoint": 51.77,
        // "humidity": 0.73,
        // "windSpeed": 13.45,
        // "windBearing": 326,
        // "cloudCover": 0.29,
        var pressure: Double = 0.toDouble()
        // "ozone": 308.9
    }
}