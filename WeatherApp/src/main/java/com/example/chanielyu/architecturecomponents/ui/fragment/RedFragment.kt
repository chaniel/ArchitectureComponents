package com.example.chanielyu.architecturecomponents.ui.fragment


import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.chanielyu.architecturecomponents.R
import com.example.chanielyu.architecturecomponents.viewmodel.NavigationViewModel
import kotlinx.android.synthetic.main.fragment_red.*

/**
 * A simple [Fragment] subclass.
 *
 */
class RedFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_red, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val navigationViewModel = ViewModelProviders.of(activity!!)[NavigationViewModel::class.java]
        toWeather.setOnClickListener({
            navigationViewModel.navigationObservable.value = WeatherFragment::class
        })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @return A new instance of fragment RedFragment.
         */
        @JvmStatic
        fun newInstance() = RedFragment()
    }

}
