package com.example.chanielyu.architecturecomponents.ui.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.chanielyu.architecturecomponents.R
import com.example.chanielyu.architecturecomponents.ui.fragment.RedFragment
import com.example.chanielyu.architecturecomponents.ui.fragment.WeatherFragment
import com.example.chanielyu.architecturecomponents.viewmodel.NavigationViewModel

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (null == savedInstanceState) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.mainContainer, WeatherFragment.newInstance("WeatherType"), WeatherFragment::class.java.simpleName)
                    .commit()
        }
        val navigationViewModel = ViewModelProviders.of(this)[NavigationViewModel::class.java]
        navigationViewModel.navigationObservable.observe(this, Observer { kClass ->
            val curFragment = supportFragmentManager.findFragmentById(R.id.mainContainer)
            if (kClass != curFragment::class) {
                val fragment = when (kClass) {
                    WeatherFragment::class -> WeatherFragment.newInstance(WeatherFragment::class.java.simpleName)
                    RedFragment::class -> RedFragment.newInstance()
                    else -> null
                }
                if (null != fragment) {
                    supportFragmentManager.beginTransaction()
                            .replace(R.id.mainContainer, fragment)
                            .commit()
                }
            }
        })
    }
}
