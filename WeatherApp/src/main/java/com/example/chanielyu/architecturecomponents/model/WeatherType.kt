package com.example.chanielyu.architecturecomponents.model

import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import com.example.chanielyu.architecturecomponents.R

enum class WeatherType(val value: String, @param:DrawableRes @field:DrawableRes
val icon: Int, @param:ColorRes val bkColor: Int) {
    CLEAR_DAY("clear-day", R.drawable.sunny, R.color.clearDayBackground),
    CLEAR_NIGHT("clear-night", R.drawable.clear_night, R.color.clearNightBackground),
    CLOUDY("cloudy", R.drawable.cloudy, R.color.cloudyBackground),
    RAIN("rain", R.drawable.rainy, R.color.rainBackground),
    SNOW("snow", R.drawable.snowy, R.color.snowBackground),
    SLEET("sleet", R.drawable.windy, R.color.windBackground),
    WIND("wind", R.drawable.windy, R.color.windBackground),
    FOG("fog", R.drawable.snowy, R.color.snowBackground),
    PARTLY_CLOUDY_DAY("partly-cloudy-day", R.drawable.sun_cloudy, R.color.errorBackground),
    PARTLY_CLOUDY_NIGHT("partly-cloudy-night", R.drawable.cloudy_night, R.color.errorBackground),
    UNKNOWN("clear-day", R.drawable.sunny, R.color.clearDayBackground);

    companion object {

        fun getWeatherType(weatherType: String): WeatherType {
            for (weather in WeatherType.values()) {
                if (weatherType.contains(weather.value))
                    return weather
            }
            return CLEAR_DAY
        }
    }
}