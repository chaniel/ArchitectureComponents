package com.example.chanielyu.architecturecomponents.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.example.chanielyu.architecturecomponents.R
import com.example.chanielyu.architecturecomponents.model.WeatherModel
import com.example.chanielyu.architecturecomponents.model.WeatherType
import com.example.chanielyu.architecturecomponents.utils.W2C
import java.text.SimpleDateFormat
import java.util.*

class WeatherRecyclerAdapter(var weatherModel: WeatherModel?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val date: Date = Date(0L)
    private val dateFormat: SimpleDateFormat = (SimpleDateFormat.getDateTimeInstance() as SimpleDateFormat).apply {
        applyPattern("yyyy-MM-dd HH:mm:ss")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.view_item_hourly, parent, false)
        return CurViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as CurViewHolder
        val summary: String? = weatherModel?.hourly?.data?.get(position)?.summary
        val time: String = dateFormat.format(date.apply {
            time = (weatherModel?.hourly?.data?.get(position)?.time ?: 0) * 1000
        })
        val temp: String = String.format(Locale.getDefault(), "%d°", W2C(weatherModel?.hourly?.data?.get(position)?.temperature
                ?: 0.0).toInt())
        val type: WeatherType = WeatherType.getWeatherType(weatherModel?.hourly?.data?.get(position)?.icon
                ?: "")

        viewHolder.summary.text = summary
        viewHolder.summary.setCompoundDrawablesWithIntrinsicBounds(type.icon, 0, 0, 0)
        viewHolder.date_time.text = time
        viewHolder.temperature.text = temp
    }

    override fun getItemCount(): Int {
        return weatherModel?.hourly?.data?.size ?: 0
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    internal inner class CurViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.hourly_summary)
        lateinit var summary: TextView
        @BindView(R.id.date_time)
        lateinit var date_time: TextView
        @BindView(R.id.temperature)
        lateinit var temperature: TextView

        init {
            ButterKnife.bind(this, itemView)
        }
    }

    fun getData(pos: Int): WeatherModel.WeatherDetails? {
        return weatherModel?.hourly?.data?.get(pos)
    }
}
