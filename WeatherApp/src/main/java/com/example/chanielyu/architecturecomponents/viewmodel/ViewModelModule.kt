package com.example.chanielyu.architecturecomponents.viewmodel

import com.example.chanielyu.architecturecomponents.retrofitclient.RetrofitClientModule
import com.example.chanielyu.architecturecomponents.retrofitclient.WeatherRetrofitClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [RetrofitClientModule::class])
class ViewModelModule {
    @Singleton
    @Provides
    fun provideViewModelFactory(weatherRetrofitClient: WeatherRetrofitClient): ViewModelFactory {
        return ViewModelFactory(weatherRetrofitClient)
    }
}