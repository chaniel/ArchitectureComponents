package com.example.chanielyu.architecturecomponents.dagger


import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FragmentScope
