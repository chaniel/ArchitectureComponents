package com.example.chanielyu.architecturecomponents.ui.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.chanielyu.architecturecomponents.R
import com.example.chanielyu.architecturecomponents.model.WeatherModel
import com.example.chanielyu.architecturecomponents.ui.adapter.WeatherRecyclerAdapter
import com.example.chanielyu.architecturecomponents.viewmodel.DaggerViewModuleComponent
import com.example.chanielyu.architecturecomponents.viewmodel.NavigationViewModel
import com.example.chanielyu.architecturecomponents.viewmodel.ViewModelFactory
import com.example.chanielyu.architecturecomponents.viewmodel.WeatherViewModel
import kotlinx.android.synthetic.main.fragment_weather.*
import javax.inject.Inject

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM = "param"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the [WeatherFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class WeatherFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private var param: String? = null
    private val recyclerAdapter = WeatherRecyclerAdapter(null)
    private lateinit var weatherViewModel: WeatherViewModel

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        DaggerViewModuleComponent.builder().build().inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param = it.getString(ARG_PARAM)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_weather, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipe.setOnRefreshListener(this)
        recycleView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recycleView.adapter = recyclerAdapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        weatherViewModel = ViewModelProviders.of(this, viewModelFactory)[WeatherViewModel::class.java]
        weatherViewModel.observableWeather.observe(this, Observer { weather ->
            onData(weather)
        })
        weatherViewModel.observableProgress.observe(this, Observer { busy ->
            swipe.isRefreshing = busy ?: false
        })
        val navigationViewModel = ViewModelProviders.of(activity!!, viewModelFactory)[NavigationViewModel::class.java]
        toRed.setOnClickListener(
                {
                    navigationViewModel.navigationObservable.value = RedFragment::class
                })

    }

    override fun onRefresh() {
        weatherViewModel.loadWeather()
    }

    private fun onData(weather: WeatherModel?) {
        recyclerAdapter.weatherModel = weather
        recyclerAdapter.notifyDataSetChanged()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param Parameter.
         * @return A new instance of fragment WeatherFragment.
         */
        @JvmStatic
        fun newInstance(param: String) =
                WeatherFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM, param)
                    }
                }
    }
}
