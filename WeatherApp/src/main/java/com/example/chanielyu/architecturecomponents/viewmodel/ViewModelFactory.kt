package com.example.chanielyu.architecturecomponents.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.chanielyu.architecturecomponents.retrofitclient.WeatherRetrofitClient

class ViewModelFactory constructor(private val weatherRetrofitClient: WeatherRetrofitClient) : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(WeatherViewModel::class.java) -> WeatherViewModel(weatherRetrofitClient) as T
            modelClass.isAssignableFrom(NavigationViewModel::class.java) -> NavigationViewModel() as T
            else -> throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
        }
    }
}