package com.example.chanielyu.architecturecomponents.utils

fun W2C(tW: Double): Double {
    return (tW - 32) * 5 / 9
}