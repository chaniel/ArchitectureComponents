package com.example.chanielyu.architecturecomponents.viewmodel

import com.example.chanielyu.architecturecomponents.dagger.FragmentScope
import com.example.chanielyu.architecturecomponents.ui.fragment.WeatherFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ViewModelModule::class])
interface ViewModuleComponent {
    fun inject(fragment: WeatherFragment)
}