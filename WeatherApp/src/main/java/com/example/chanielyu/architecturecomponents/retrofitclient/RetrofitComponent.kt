package com.example.chanielyu.architecturecomponents.retrofitclient

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RetrofitClientModule::class])
interface RetrofitComponent {
    fun weatherRetrofitClient(): WeatherRetrofitClient
}