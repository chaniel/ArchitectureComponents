package com.example.chanielyu.architecturecomponents.retrofitclient

import com.example.chanielyu.architecturecomponents.model.WeatherModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface WeatherRetrofitClient {
    @GET("{latitude},{longitude}")
    abstract fun getWeatherRx(@Path("latitude") latitude: String, @Path("longitude") longitude: String): Single<WeatherModel>
}